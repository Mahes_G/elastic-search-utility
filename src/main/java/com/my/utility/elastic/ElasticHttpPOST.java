package com.my.utility.elastic;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

public class ElasticHttpPOST {

	private static final String INDEX_NAME = "test-index";
	private static final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) throws Exception {

		String elasticSearchIndexPath = "http://localhost:9200/" + INDEX_NAME.toLowerCase() + "/data/";
		boolean useFixedIndex = true;
		String urlWithIndex = elasticSearchIndexPath;

		JSONParser jsonParser = new JSONParser();
		URL resource = ElasticHttpPOST.class.getResource("/data.json");
		System.out.println("Input JSON Path:" + resource.getPath());

		try (FileReader reader = new FileReader(resource.getPath())) {
			// Read JSON file
			Object parsedJsonArrayObj = jsonParser.parse(reader);

			JSONArray jsonArray = (JSONArray) parsedJsonArrayObj;

			for (int i = 0; i < jsonArray.size(); i++) {
				if (useFixedIndex) {
					urlWithIndex = elasticSearchIndexPath + (i + 1);
				}
				System.out.println(">>>>>>>>> >>>>>>>>> >>>>>>>>> >>>>>>>>> >>>>>>>>> >>>>>>>>> ");
				System.out.println("\nCalling 'POST' request to URL : " + urlWithIndex);

				// System.out.println(jsonArray.get(i).toString());
				sendPost(urlWithIndex, jsonArray.get(i).toString());

				System.out.println("<<<<<<<<< <<<<<<<<< <<<<<<<<< <<<<<<<<< <<<<<<<<< <<<<<<<<< ");
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// HTTP POST request
	private static void sendPost(String updatedElasticSearchIndexPath, String indObj) throws Exception {

		URL httpUrl = new URL(updatedElasticSearchIndexPath);
		HttpURLConnection con = (HttpURLConnection) httpUrl.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type", "application/json");

		// Send post request
		con.setDoOutput(true);

		System.out.println("--------- --------- --------- --------- --------- --------- ");
		System.out.println("Post body : ");
		System.out.println("--------- --------- --------- --------- --------- --------- ");
		System.out.println(indObj);
		System.out.println("--------- --------- --------- --------- --------- --------- ");

		con.setRequestProperty("Content-length", String.valueOf(indObj.length()));

		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(indObj);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("Response Code : " + responseCode);

		BufferedReader httpResStream = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String responseLine;
		StringBuffer httpResponse = new StringBuffer();

		while ((responseLine = httpResStream.readLine()) != null) {
			httpResponse.append(responseLine);
		}
		httpResStream.close();

		// print result
		System.out.println("--------- --------- --------- --------- --------- --------- ");
		System.out.println("response : ");
		System.out.println("--------- --------- --------- --------- --------- --------- ");
		System.out.println(httpResponse.toString());
		System.out.println("--------- --------- --------- --------- --------- --------- ");
	}

}
